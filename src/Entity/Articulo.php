<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ArticuloRepository")
 */
class Articulo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Debes introducir un nombre para el artículo.")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=510)
     * @Assert\NotBlank(message="Debes introducir una descripción.")
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float", length=255)
     * @Assert\Positive(
     *     message="El precio debe ser positivo."
     * )
     * @Assert\NotBlank(message="Debes introducir un precio.")
     */
    private $precio;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Debes introducir una categoría.")
     */
    private $categoria;

    /**
     * @Assert\NotBlank(message="Debes introducir un estado.")
     * @ORM\Column(type="string", length=255)
     */
    private $estado;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imagen;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     mimeTypes={ "image/png", "image/jpeg" },
     *     mimeTypesMessage="Tipo de imagen no permitido, solo se permite PNG y JPG"
     * )
     */
    private $imagenFile;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $fecha;

    /**
     * @var Usuario
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     */
    private $usuario;

    /**
     * Articulo constructor.
     * @param $fecha
     */
    public function __construct()
    {
        $this->fecha = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    public function setPrecio(string $precio): self
    {
        $this->precio = $precio;

        return $this;
    }

    public function getCategoria(): ?string
    {
        return $this->categoria;
    }

    public function setCategoria(string $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }

    public function getEstado(): ?string
    {
        return $this->estado;
    }

    public function setEstado(string $estado): self
    {
        $this->estado = $estado;

        return $this;
    }

    public function getImagen(): ?string
    {
        return $this->imagen;
    }

    public function setImagen(string $imagen): self
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getImagenFile(): ?UploadedFile
    {
        return $this->imagenFile;
    }

    /**
     * @param UploadedFile $imagenFile
     * @return Articulo
     */
    public function setImagenFile(UploadedFile $imagenFile): Articulo
    {
        $this->imagenFile = $imagenFile;
        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }
}
