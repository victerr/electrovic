<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario implements UserInterface, \Serializable
{
    /**
     * Usuario constructor.
     * @param $fecha_registro
     */
    public function __construct()
    {
        $this->fechaRegistro = new \DateTime();
        $this->setRole('ROLE_USER');
        $this->setRAngo('Noob');
    }

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El nombre de usuario es obligatorio.")
     * * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "El nombre debe tener al menos {{ limit }} caracteres.",
     *      maxMessage = "El nombre debe tener como máximo {{ limit }} caracteres."
     * )
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * * @Assert\NotBlank(message="La contraseña es obligatoria.")
     * * @Assert\Length(
     *      min = 5,
     *      max = 50,
     *      minMessage = "El nombre debe tener al menos {{ limit }} caracteres.",
     *      maxMessage = "El nombre debe tener como máximo {{ limit }} caracteres."
     * )
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     * * @Assert\NotBlank(message="El nombre de usuario es obligatorio.")
     * @Assert\Email(
     *     message = "'{{ value }}' no es un email válido."
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $avatar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rango;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $fechaRegistro;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     mimeTypes={ "image/png", "image/jpeg" },
     *     mimeTypesMessage="Tipo de imagen no permitido, solo se permite PNG y JPG"
     * )
     */
    private $avatarFile;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getRango(): ?string
    {
        return $this->rango;
    }

    public function setRango(string $rango): self
    {
        $this->rango = $rango;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getAvatarFile(): ?UploadedFile
    {
        return $this->avatarFile;
    }

    /**
     * @param UploadedFile $avatarFile
     * @return Usuario
     */
    public function setAvatarFile(UploadedFile $avatarFile): Usuario
    {
        $this->avatarFile = $avatarFile;
        return $this;
    }

    public function getFechaRegistro(): ?\DateTimeInterface
    {
        return $this->fechaRegistro;
    }

    public function setFechaRegistro(\DateTimeInterface $fechaRegistro): self
    {
        $this->fecha_registro = $fechaRegistro;

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return [ $this->role ];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->avatar
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list(
            $this->id,
            $this->username,
            $this->password,
            $this->email,
            $this->avatar
            ) = unserialize($serialized);
    }
}
