<?php

namespace App\BLL;

use App\Entity\Articulo;
use App\Entity\Usuario;
use App\Helper\FileUploader;
use App\Repository\ArticuloRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ArticuloBLL extends BaseBLL
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function eliminar(Articulo $articulo)
    {
        $this->entityManager->remove($articulo);
        $this->entityManager->flush();
    }

    public function guarda(Articulo $articulo, UploadedFile $imgFoto=null)
    {
        if (!is_null($imgFoto))
        {
            $fileName = $this->uploader->upload($imgFoto);

            $articulo->setImagen($fileName);
        }

        $articulo->setUsuario($this->getUser());

        $this->entityManager->persist($articulo);
        $this->entityManager->flush();

        return $articulo;
    }

    /**
     * @param Request $request
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function nuevo(Request $request, array $data)
    {
        if (!isset($data['nombre']) || !isset($data['descripcion']) || !isset($data['precio']) || !isset($data['estado']) || !isset($data['categoria']) )
            throw new BadRequestHttpException('Faltan datos por rellenar');

        $articulo = new Articulo();
        $articulo->setNombre($data['nombre']);
        $articulo->setDescripcion($data['descripcion']);
        $articulo->setPrecio($data['precio']);
        $articulo->setEstado($data['estado']);
        $articulo->setCategoria($data['categoria']);

        $usuario = $this->getUser();
        $articulo->setUsuario($usuario);

        if (isset($data['imagen']) && !empty($data['imagen']))
        {
            $fotos_upload_dir = $this->params->get('fotos_upload_dir');
            $fotos_url_directory = $this->params->get('fotos_url_directory');

            $arr_foto = explode(',', $data['imagen']);
            if (count($arr_foto) < 2)
                throw new BadRequestHttpException('Formato de imagen incorrecto');

            $imgFoto = base64_decode($arr_foto[1]);
            if (!is_null($imgFoto))
            {
                $fileName = md5(uniqid()).'.png';
                $filePath = $fotos_url_directory . $fileName;
                $urlAvatar = $request->getUriForPath($filePath);
                $articulo->setImagen($fileName);

                $ifp = fopen($fotos_upload_dir . '/' . $fileName, "wb");
                if ($ifp)
                {
                    $ok = fwrite($ifp, $imgFoto);
                    if (!$ok)
                        throw new \Exception('No se ha podido cargar la imagen del avatar');

                    fclose($ifp);
                }
            }
            else
                throw new \Exception('No se ha podido cargar la imagen del avatar');
        }

        return $this->guardaValidando($articulo);
    }

    public function getArticulosFiltrados(
        string $order, string $nombre=null, string $descripcion=null)
    {
        $usuario = $this->getUser();

        /** @var ArticuloRepository $articuloRepository */
        $articuloRepository = $this->entityManager->getRepository(Articulo::class);

        $articulos = $articuloRepository->getArticulosFiltrados(
            $order, $usuario->getId(), $nombre, $descripcion);

        return $this->entitiesToArray($articulos);
    }

    public function getAll()
    {
        $articulos = $this->entityManager->getRepository(Articulo::class)->findAll();

        return $this->entitiesToArray($articulos);
    }

    public function update(Articulo $articulo, array $data)
    {
        $articulo->setNombre($data['nombre']);
        $articulo->setDescripcion($data['descripcion']);
        $articulo->setPrecio($data['precio']);
        $articulo->setEstado($data['estado']);
        $articulo->setCategoria($data['categoria']);

        return $this->guardaValidando($articulo);
    }

    public function toArray($articulo) : array
    {
        return [
            'id' => $articulo->getId(),
            'nombre' => $articulo->getNombre(),
            'descripcion' => $articulo->getDescripcion(),
            'precio' => $articulo->getPrecio(),
            'estado' => $articulo->getEstado(),
            'categoria' => $articulo->getCategoria()
        ];
    }
}