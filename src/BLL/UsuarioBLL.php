<?php


namespace App\BLL;

use App\Entity\Usuario;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

use App\Helper\FileUploader;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioBLL extends BaseBLL
{
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }


    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    /**
     * @required
     */
    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    /**
     * @required
     */
    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function guarda(Usuario $usuario, UploadedFile $imgFoto=null)
    {
        if (!is_null($imgFoto))
        {
            $fileName = $this->uploader->upload($imgFoto);

            $usuario->setAvatar($fileName);
        }

        $this->entityManager->persist($usuario);
        $this->entityManager->flush();

        return $usuario;
    }

    public function edita(Usuario $usuario, string $nuevoPassword=null, UploadedFile $imgFoto=null)
    {
        if (!is_null($imgFoto))
        {
            $fileName = $this->uploader->upload($imgFoto);

            $usuario->setAvatar($fileName);
        }

        if (!is_null($nuevoPassword))
        {
            $usuario->setPassword($this->encoder->encodePassword($usuario, $nuevoPassword));
        }


        $this->entityManager->persist($usuario);
        $this->entityManager->flush();

        return $usuario;
    }

    public function getAll() : array
    {
        $usuarios = $this->entityManager->getRepository(Usuario::class)->findAll();

        return $this->entitiesToArray($usuarios);
    }

    public function nuevo(string $username, string $password) : array
    {
        $user = new Usuario();

        $user->setUsername($username);/*
        $user->setEmail($email);*/
        $user->setPassword($this->encoder->encodePassword($user, $password));


        return $this->guardaValidando($user);
    }

    public function profile() : array
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }


    public function toArray($usuario) : array
    {
        return [
            'id' => $usuario->getId(),
            'username' => $usuario->getUsername(),
            'email' => $usuario->getEmail(),
            'role' => $usuario->getRole(),
            'rango' => $usuario->getRango()
        ];
    }

    public function getTokenByEmail(string $email)
    {
        $usuario = $this->entityManager->getRepository(Usuario::class)
            ->findOneBy(array('email'=>$email));

        if (is_null($usuario))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($usuario);
    }

}