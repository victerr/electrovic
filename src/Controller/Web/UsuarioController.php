<?php

namespace App\Controller\Web;

use App\BLL\UsuarioBLL;
use App\Entity\Articulo;
use App\Entity\Usuario;
use App\Form\UsuarioAdminType;
use App\Form\UsuarioType;
use App\Repository\ArticuloRepository;
use App\Repository\UsuarioRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/")
 */
class UsuarioController extends AbstractController
{
    private $encoder;
    private $session;
    private $cont = 0;


    public function __construct( UserPasswordEncoderInterface $encoder, SessionInterface $session){
        $this->encoder = $encoder;
        $this->session = $session;
    }
    /**
     * @Route("/admin/usuarios", name="usuario_index", methods={"GET"})
     */
    public function index(UsuarioRepository $usuarioRepository): Response
    {
        return $this->render('usuario/index.html.twig', [
            'usuarios' => $usuarioRepository->findAll(),
        ]);
    }

    /**
     * @Route("/usuarios/{id}/articulos", name="usuario_articulos", methods={"GET"})
     */
    public function getArticulos(UserInterface $user)
    {
        $userId = $user->getId();

        return $this->render('articulo/index.html.twig', [
            'articulos' => $this->getDoctrine()->getRepository(Articulo::class)->findBy(['usuario' => $userId])
        ]);
    }

    /**
     * @Route("/registro", name="registro", methods={"GET"})
     */
    public function new(Request $request): Response
    {
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioType::class, $usuario,['attr' => ['class' => 'form-horizontal'], 'action' => $this->generateUrl("usuario_guardar")]);
        $form->handleRequest($request);

        return $this->render('usuario/new.html.twig',
            [
                'articulos' => $this->getDoctrine()->getRepository(Usuario::class)->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/admin/usuarios/new", name="usuario_admin_new", methods={"GET"})
     */
    public function newAdmin(Request $request): Response
    {
        $usuario = new Usuario();
        $form = $this->createForm(UsuarioAdminType::class, $usuario,['attr' => ['class' => 'form-horizontal'], 'action' => $this->generateUrl("usuario_guardar_admin")]);
        $form->handleRequest($request);

        return $this->render('usuario/newAdmin.html.twig',
            [
                'articulos' => $this->getDoctrine()->getRepository(Usuario::class)->findAll(),
                'form' => $form->createView()
            ]
        );
    }


    /**
     * @Route(
     *     "/usuario/guardar",
     *     name="usuario_guardar",
     *     methods={"POST"}
     * )
     */
    public function guarda(Request $request, UsuarioBLL $usuarioBLL)
    {
        $usuario = new Usuario();

        $form = $this->createForm(
            UsuarioType::class,
            $usuario,
            [
                'action' => $this->generateUrl("usuario_guardar_admin")
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('avatarFile')->getData();
            $password = $form->get('password')->getData();

            $usuario->setPassword($this->encoder->encodePassword($usuario, $password));

            $usuarioBLL->guarda($usuario, $imgFoto);

            $this->addFlash(
                'notice',
                'El usuario se ha añadido correctamente!'
            );

            return $this->redirectToRoute('usuario_index');
        }

        return $this->render(
            'usuario/new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/admin/usuario/guardar",
     *     name="usuario_guardar_admin",
     *     methods={"POST"}
     * )
     */
    public function guarda_admin(Request $request, UsuarioBLL $usuarioBLL)
    {
        $usuario = new Usuario();

        $form = $this->createForm(
            UsuarioAdminType::class,
            $usuario,
            [
                'action' => $this->generateUrl("usuario_guardar_admin")
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('avatarFile')->getData();
            $password = $form->get('password')->getData();

            $usuario->setPassword($this->encoder->encodePassword($usuario, $password));

            $usuarioBLL->guarda($usuario, $imgFoto);

            $this->addFlash(
                'notice',
                'El usuario se ha añadido correctamente!'
            );

            return $this->redirectToRoute('usuario_index');
        }

        return $this->render(
            'usuario/newAdmin.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/usuarios/{id}/actualizar",
     *     name="usuario_actualizar",
     *     methods={"POST"}
     * )
     */
    public function actualiza(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario)
    {
        $form = $this->createForm(
            UsuarioType::class,
            $usuario,
            [
                'action' => $this->generateUrl("usuario_actualizar", [ 'id' => $usuario->getId() ] )
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('avatarFile')->getData();
            $password = $form->get('password')->getData();

            $usuario->setPassword($this->encoder->encodePassword($usuario, $password));

            $usuarioBLL->guarda($usuario, $imgFoto);

            $this->addFlash(
                'notice',
                'El usuario se ha modificado correctamente!'
            );

            return $this->redirectToRoute('usuario_show', [ 'id' => $usuario->getId() ] );
        }

        return $this->render(
            'usuario/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/admin/usuarios/{id}/actualizar",
     *     name="usuario_actualizar_admin",
     *     methods={"POST"}
     * )
     */
    public function actualiza_admin(Request $request, UsuarioBLL $usuarioBLL, Usuario $usuario)
    {
        $form = $this->createForm(
            UsuarioAdminType::class,
            $usuario,
            [
                'action' => $this->generateUrl("usuario_actualizar_admin", [ 'id' => $usuario->getId() ] )
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('avatarFile')->getData();

            $usuarioBLL->guarda($usuario, $imgFoto);

            $this->addFlash(
                'notice',
                'El usuario se ha modificado correctamente!'
            );

            return $this->redirectToRoute('usuario_show', [ 'id' => $usuario->getId() ] );
        }

        return $this->render(
            'usuario/editAdmin.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }


    /**
     * @Route(
     *     "/carro",
     *     name="carro",
     *     methods={"GET", "POST"}
     * )
     */
    public function carro(ArticuloRepository $articuloRepository) {

        $total = 0;

        $carro = $this->session->get('carro');

        if(is_null($carro)){
            $carro = [];
        }

        foreach ($carro as $key => &$articulo){
            $total+=$articulo->getPrecio();
        }

        return $this->render(
            'usuario/carro.html.twig',
            [
                'carro' => $carro,
                'total' => $total
            ]
        );
    }

    /**
     * @Route(
     *     "/carro/comprar",
     *     name="carro_comprar",
     *     methods={"GET"}
     * )
     */
    public function carro_comprar()
    {
        $this->session->remove('carro');

        return $this->render(
            'usuario/carro.html.twig',
            [
                'carro' => null,
                'total' => 0
            ]
        );
    }

    /**
     * @Route(
     *     "/carro/{id}",
     *     name="carro_add",
     *     methods={"GET"}
     * )
     */
    public function carroAdd(Request $request, ArticuloRepository $articuloRepository){

        $id=$request->get('id');
        $total = 0;
        $articulo = $articuloRepository->find($id);

        $carro = $this->session->get('carro');

        if(is_null($carro)){
            $this->session->set('carro', []);
        }
        else{
            $this->session->set('carro', $carro);
        }

        $carro = $this->session->get('carro');

        array_push($carro, $articulo);

        $this->session->set('carro', $carro);


        return $this->redirectToRoute('carro');
    }



    /**
     * @Route("/carro/{id}/delete",
     *     name="carro_delete",
     *     methods={"GET"})
     */
    public function carroDelete(Request $request): Response
    {
        $id=$request->get('id');
        $carro = $this->session->get('carro');

       foreach ($carro as $key => &$articulo){
           $articuloID=$articulo->getId();
           if($articuloID == $id){
               unset($carro[$key]);
           }
       }

        $this->session->set('carro', $carro);

        return $this->redirectToRoute('carro');
    }

    /**
     * @Route("/usuarios/{id}", name="usuario_show", methods={"GET"})
     */
    public function show(Usuario $usuario): Response
    {
        return $this->render('usuario/show.html.twig', [
            'usuario' => $usuario,
        ]);
    }

    /**
     * @Route("/admin/usuarios/{id}/edit", name="usuario_admin_edit", methods={"GET"})
     */
    public function editAdmin(Request $request, Usuario $usuario): Response
    {
        $form = $this->createForm(UsuarioAdminType::class, $usuario,['attr' => ['class' => 'form-horizontal'], 'action' => $this->generateUrl("usuario_actualizar_admin", [ 'id' => $usuario->getId() ])]);
        $form->handleRequest($request);

        return $this->render('usuario/editAdmin.html.twig',
            [
                'usuario' => $usuario,
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/usuarios/{id}/edit", name="usuario_edit", methods={"GET"})
     */
    public function edit(Request $request, Usuario $usuario): Response
    {
        $form = $this->createForm(UsuarioType::class, $usuario,['attr' => ['class' => 'form-horizontal'], 'action' => $this->generateUrl("usuario_actualizar", [ 'id' => $usuario->getId() ] )]);


        return $this->render('usuario/edit.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/{id}", name="usuario_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Usuario $usuario): Response
    {
        if ($this->isCsrfTokenValid('delete'.$usuario->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($usuario);
            $entityManager->flush();
        }

        return $this->redirectToRoute('usuario_index');
    }

}
