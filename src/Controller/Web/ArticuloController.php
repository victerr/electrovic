<?php

namespace App\Controller\Web;

use App\BLL\ArticuloBLL;
use App\Entity\Articulo;
use App\Form\ArticuloType;
use App\Repository\ArticuloRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/articulos")
 */
class ArticuloController extends AbstractController
{
    /**
     * @Route("/", name="articulo_listar", methods={"GET"})
     */
    public function index(ArticuloRepository $articuloRepository): Response
    {
        return $this->render('articulo/index.html.twig', [
            'articulos' => $this->getDoctrine()->getRepository(Articulo::class)->findAll()
        ]);
    }

    /**
     * @Route("/categoria/{categoria}", name="articulo_listar_categoria", methods={"GET"})
     * @param ArticuloRepository $articuloRepository
     * @param $request
     * @return Response
     */
    public function filtrarPorCategoria(ArticuloRepository $articuloRepository, Request $request): Response
    {
        $categoria=$request->get('categoria');

        return $this->render('articulo/index.html.twig', [
            'articulos' => $this->getDoctrine()->getRepository(Articulo::class)->findBy(['categoria' => $categoria ])
        ]);
    }

    /**
     * @Route(
     *     "/articulos/guardar",
     *     name="articulo_guardar",
     *     methods={"POST"}
     * )
     */
    public function guarda(Request $request, ArticuloBLL $articuloBLL)
    {
        $articulo = new Articulo();

        $form = $this->createForm(
            ArticuloType::class,
            $articulo,
            [
                'action' => $this->generateUrl("articulo_guardar")
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('imagenFile')->getData();

            $articuloBLL->guarda($articulo, $imgFoto);

            $this->addFlash(
                'notice',
                'El artículo se ha añadido correctamente!'
            );

            return $this->redirectToRoute('articulo_listar');
        }

        return $this->render(
            'articulo/new.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/new",
     *     name="articulo_new",
     *     methods={"GET"})
     */
    public function new(Request $request): Response
    {
        $articulo = new Articulo();

        $form = $this->createForm(
            ArticuloType::class,
            $articulo,
            [
                'action' => $this->generateUrl("articulo_guardar")
            ]
        );

        return $this->render('articulo/new.html.twig',
            [
                'articulos' => $this->getDoctrine()->getRepository(Articulo::class)->findAll(),
                'form' => $form->createView()
            ]
        );
    }



    /**
     * @Route("/{id}", name="articulo_show", methods={"GET"})
     */
    public function show(Articulo $articulo): Response
    {
        return $this->render('articulo/show.html.twig', [
            'articulo' => $articulo,
        ]);
    }

    /**
     * @Route(
     *     "/articulos/{id}/actualizar",
     *     name="articulo_actualizar",
     *     methods={"POST"}
     * )
     */
    public function actualiza(Request $request, ArticuloBLL $articuloBLL, Articulo $articulo)
    {
        $form = $this->createForm(
            ArticuloType::class,
            $articulo,
            [
                'action' => $this->generateUrl("articulo_actualizar", [ 'id' => $articulo->getId() ] )
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('imagenFile')->getData();

            $articuloBLL->guarda($articulo, $imgFoto);

            $this->addFlash(
                'notice',
                'El artículo se ha guardado correctamente!'
            );

            return $this->redirectToRoute('articulo_listar');
        }

        return $this->render(
            'articulo/editar.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }


    /**
     * @Route("/{id}/edit",
     *     name="articulo_edit",
     *     methods={"GET"}
     *     )
     */
    public function edit(Request $request, Articulo $articulo): Response
    {
        $form = $this->createForm(ArticuloType::class, $articulo,
            [
                'action' => $this->generateUrl("articulo_actualizar", [ 'id' => $articulo->getId() ] )
            ]);

        return $this->render('articulo/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete",
     *     name="articulo_delete",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     *     )
     */
    public function delete(Request $request, Articulo $articulo, ArticuloBLL $articuloBLL): Response
    {
        $id = $articulo->getId();

        $articuloBLL->eliminar($articulo);

        $this->addFlash(
            'notice',
            "El artículo $id se ha eliminado correctamente"
        );

        return $this->redirectToRoute('articulo_listar');

    }

    /**
     * @Route(
     *     "/buscar",
     *     name="articulo_buscar",
     *     methods={"POST"}
     * )
     */
    public function buscar(Request $request, ArticuloBLL $articuloBLL)
    {
        $busqueda = $request->request->get('busqueda');
        $categoria = $request->request->get('categoria');


        /** @var ArticuloRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Articulo::class);

        //$articulo = $repository->find(intval($busqueda));
/*
        if (is_null($articulo))
            $articulos = $repository->findArticulo($busqueda);
        else
            $articulos = [ $articulo ];*/

        $articulos =  $repository->findArticulo($busqueda, $categoria);

        return $this->render(
            'articulo/index.html.twig',
            [
                'articulos' => $articulos
            ]
        );
    }
}
