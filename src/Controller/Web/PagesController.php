<?php

namespace App\Controller\Web;

use App\Repository\ArticuloRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route(
     *     "/",
     *     name="electrovic_pages_inicio",
     *     methods={"GET", "POST"}
     * )
     */
    public function inicio(ArticuloRepository $articuloRepository)
    {
        $nombre = 'Álex';
        return $this->render(
            'articulo/index.html.twig',
            [
                'articulos' => $articuloRepository->findAll(),
            ]
        );
    }

}