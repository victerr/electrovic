<?php


namespace App\Controller\Rest\v1;

use App\BLL\UsuarioBLL;
use App\Controller\Rest\BaseApiController;
use App\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UsuarioRestController extends BaseApiController
{
    /**
     * @Route(
     *     "/usuarios.{_format}",
     *     name="electrovic_apiv1_get_usuarios",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     *     methods={"GET"}
     * )
     */
    public function getAll(UsuarioBLL $usuarioBLL)
    {
        $result = $usuarioBLL->getAll();

        return $this->getResponse($result);
    }

    /**
     * @Route(
     *     "/usuarios/{id}.{_format}",
     *     name="electrovic_apiv1_get_usuario",
     *     defaults={"_format": "json"},
     *     requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *     },
     *     methods={"GET"}
     * )
     */
    public function getOne(Usuario $usuario, UsuarioBLL $usuarioBLL)
    {
        return $this->getResponse($usuarioBLL->toArray($usuario));
    }

    /**
     * @Route(
     *     "/usuarios/{id}.{_format}",
     *     name="electrovic_apiv1_delete_usuario",
     *     defaults={"_format": "json"},
     *     requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *     },
     *     methods={"DELETE"}
     * )
     */
    public function delete(Usuario $usuario, UsuarioBLL $usuarioBLL)
    {
        $usuarioBLL->delete($usuario);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route(
     *     "/profile.{_format}",
     *     name="electrovic_apiv1_profile",
     *     defaults={"_format": "json"},
     *     requirements={
     *          "_format": "json"
     *     },
     *     methods={"GET"}
     * )
     */
    public function profile(UsuarioBLL $usuarioBLL)
    {
        $usuario = $usuarioBLL->profile();

        return $this->getResponse($usuario);
    }
}