<?php


namespace App\Controller\Rest\v1;

use App\BLL\ArticuloBLL;
use App\Controller\Rest\BaseApiController;
use App\Entity\Articulo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticuloRestController extends BaseApiController
{
    /**
     * @Route(
     *     "/articulos.{_format}",
     *     name="electrovic_apiv1_get_articulos",
     *     methods={"GET"}
     * )
     */
    public function getAll(Request $request, ArticuloBLL $articuloBLL)
    {
        return $this->getResponse($articuloBLL->getAll());
    }

    /**
     * @Route(
     *     "/articulos/{id}.{_format}",
     *     name="electrovic_apiv1_get_articulo",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"GET"}
     * )
     */
    public function getOne(Articulo $articulo, ArticuloBLL $articuloBLL)
    {
        return $this->getResponse($articuloBLL->toArray($articulo));
    }

    /**
     * @Route(
     *     "/articulos.{_format}",
     *     name="electrovic_apiv1_post_articulos",
     *     methods={"POST"}
     * )
     */
    public function post(Request $request, ArticuloBLL $articuloBLL)
    {
        $data = $this->getContent($request);

        $articulo = $articuloBLL->nuevo($request, $data);

        return $this->getResponse($articulo, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/articulos/{id}.{_format}",
     *     name="electrovic_apiv1_put_articulos",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"PUT"}
     * )
     */
    public function update(Request $request, Articulo $articulo, ArticuloBLL $articuloBLL)
    {
        $data = $this->getContent($request);

        $articulo = $articuloBLL->update($articulo, $data);

        return $this->getResponse($articulo, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/articulos/{id}.{_format}",
     *     name="electrovic_apiv1_delete_articulos",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"DELETE"}
     * )
     */
    public function delete(Articulo $articulo, ArticuloBLL $articuloBLL)
    {
        $articuloBLL->delete($articulo);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }
}