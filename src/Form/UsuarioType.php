<?php

namespace App\Form;

use App\Entity\Usuario;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsuarioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'Contraseña'
                ],
                'second_options' => [
                    'label' => 'Repite la contraseña'
                ],
                'required' => true,
                'invalid_message' => 'Los passwords no coinciden'
            ])
            ->add('email', EmailType::class)
            ->add('avatarFile',FileType::class, [
                'label' => 'Foto (PNG/JPG)',
                'mapped' => false,
                'required' => false
            ])
            ->add('guardar', SubmitType::class,[
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-success col-md-5']
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Usuario::class,
        ]);
    }
}
