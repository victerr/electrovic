<?php

namespace App\Form;

use App\Entity\Articulo;
use App\Service\Variables;
use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArticuloType extends AbstractType
{

    private $categorias;

    /**
     * ArticuloType constructor.
     * @param $container
     */
    public function __construct(Variables $variables)
    {
        $this->categorias = $variables->getCategorias();
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('descripcion')
            ->add('precio', NumberType::class, ['invalid_message' => 'El precio debe ser un número.'])
            ->add('categoria', ChoiceType::class, [
                'choices'  => [
                    $this->categorias[0] => $this->categorias[0],
                    $this->categorias[1] => $this->categorias[1],
                    $this->categorias[2] => $this->categorias[2],
                    $this->categorias[3] => $this->categorias[3]
                ]])
            ->add('estado')
            ->add('imagenFile', FileType::class, [
                'label' => 'Foto (PNG/JPG)',
                'mapped' => false,
                'required' => false
            ])
            ->add('guardar', SubmitType::class,[
                'label' => 'Guardar',
                'attr' => ['class' => 'btn btn-success col-md-5']
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Articulo::class,
        ]);
    }
}
