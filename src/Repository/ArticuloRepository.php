<?php

namespace App\Repository;

use App\Entity\Articulo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use function Doctrine\ORM\QueryBuilder;

/**
 * @method Articulo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Articulo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Articulo[]    findAll()
 * @method Articulo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticuloRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Articulo::class);
    }

    public function getArticulosFiltrados(
        string $order, int $idUsuario, string $nombre=null, string $descripcion=null)
    {
        $qb = $this ->createQueryBuilder('articulo');

        $qb->where($qb->expr()->eq('articulo.usuario', ':usuario'))
            ->setParameter('usuario', $idUsuario);

        if (isset($nombre))
        {
            $qb->andWhere($qb->expr()->like('articulo.nombre', ':nombre'))
                ->setParameter('nombre', '%'.$nombre.'%');
        }

        if (isset($descripcion))
        {
            $qb->andWhere($qb->expr()->like('articulo.descripcion', ':descripcion'))
                ->setParameter('descripcion', '%'.$descripcion.'%');
        }

        $qb->orderBy('articulo.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }

    public function findArticulo(string $search, string $categoria)
    {
        $qb = $this->createQueryBuilder('articulo');


        if(strcasecmp($categoria, 'Todas') !== 0) {
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->like('articulo.nombre', ':search'),
                    $qb->expr()->like('articulo.descripcion', ':search')
                )
            )->andWhere(
                $qb->expr()->andX(
                    $qb->expr()->like('articulo.categoria', ':categoria')
                )
            );

            $qb->setParameter('categoria', '%' . $categoria . '%');
        }
        else {
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->like('articulo.nombre', ':search'),
                    $qb->expr()->like('articulo.descripcion', ':search')
                )
            );
        }

        $qb->setParameter('search', '%' . $search . '%');

        return $qb->getQuery()->execute();
    }
}
