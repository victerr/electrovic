<?php


namespace App\Service;


class Variables
{
    public function getCategorias()
    {
        return [
            'Móviles',
            'Portátiles',
            'Sobremesa',
            'Consolas'
        ];
    }

    public function getRangos()
    {
        return [
            'Noob',
            'Primeros pasos',
            'Vas mejorando',
            'Usuario avanzado',
            'Pro'
        ];
    }
}